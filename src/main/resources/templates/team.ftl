<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Welcome!</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div th:replace="fragments/header :: header">&nbsp;</div>
<div class="container">



    <div>
        <span> I am freemarker in Resources directory </span> <button id="clicker">Click Me</button>
    </div>
    <div id="display"></div>

</div>
</body>

</html>