package org.tmotley.footyscraper.auth;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.tmotley.footyscraper.services.FootyDataService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FootySiteAuthProvider implements AuthenticationProvider {
    private FootyDataService footyDataService;
    //todo inject from props file
    private final String LOGIN_URL = "https://users.premierleague.com/PremierUser/j_spring_security_check";
    private final String FF_USER_COOKIE_ID = "pluser";

    @Autowired
    public FootySiteAuthProvider(FootyDataService footyDataService) {
        this.footyDataService = footyDataService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        Map<String,String> authCookies = new HashMap<String,String>();
        List<GrantedAuthority> grantedAuths = new ArrayList<>();

        try {
            authCookies = footyDataService.doFormAuth(name, password);
        } catch (Exception e) {
            e.printStackTrace();
        }

        FootySiteAuthToken auth = null;
        if (authCookies != null && authCookies.containsKey(FF_USER_COOKIE_ID)) {
            grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));
            auth = new FootySiteAuthToken(name, password, grantedAuths, authCookies);
            //System.out.println("PL USER IS " + auth.getAuthCookies().get(FF_USER_COOKIE_ID));
        }

        return auth;
    }

    private Map<String,String> doFormAuth(String user, String pass) throws Exception {
       Connection.Response res = Jsoup.connect(LOGIN_URL)
                .data("j_username", user, "j_password", pass)
                .method(Connection.Method.POST)
                .validateTLSCertificates(false)
                .followRedirects(true)
                .execute();

        return res.cookies();
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}
