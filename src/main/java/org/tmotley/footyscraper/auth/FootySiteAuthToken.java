package org.tmotley.footyscraper.auth;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import javax.security.auth.Subject;
import java.util.Collection;
import java.util.Map;

public class FootySiteAuthToken extends UsernamePasswordAuthenticationToken {
    private Map<String, String> authCookies;

    public FootySiteAuthToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, Map<String, String> cookies) {
        super(principal, credentials, authorities);
        authCookies = cookies;
    }

    public Map<String, String> getAuthCookies() {
        return authCookies;
    }

    @Override
    public boolean implies(Subject subject) {
        return subject != null && subject.getPrincipals(FootySiteAuthToken.class).contains(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        FootySiteAuthToken that = (FootySiteAuthToken) o;

        return !(authCookies != null ? !authCookies.equals(that.authCookies) : that.authCookies != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (authCookies != null ? authCookies.hashCode() : 0);
        return result;
    }
}
