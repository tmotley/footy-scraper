package org.tmotley.footyscraper.services;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.tmotley.footyscraper.domain.Player;
import org.tmotley.footyscraper.domain.Team;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Tom on 8/8/15.
 */
@Component
public class FootyDataService {
    private ThreadLocal<Map<String,String>> sessionCookies = new ThreadLocal<Map<String,String>>();

    private final String homeURl = "http://fantasy.premierleague.com/";
    private final String loginUrl = "https://users.premierleague.com/PremierUser/j_spring_security_check";
    final String FF_USER_COOKIE_ID = "pluser";

    /**
     * Obtain an authorization cookie using the credentials provided
     *
     * @param user
     * @param pass
     * @return The authentication cookie collection
     * @throws Exception
     */
    public Map<String,String> doFormAuth(String user, String pass) throws Exception {
        Connection.Response res = Jsoup.connect(loginUrl)
                .data("j_username", user, "j_password", pass)
                .method(Connection.Method.POST)
                .validateTLSCertificates(false)
                .followRedirects(true)
                .execute();

        return res.cookies();
    }

    public void initSession(Map<String,String> cookieMap) {
        sessionCookies.set(cookieMap);
    }

    public void endSession() {
        sessionCookies.remove();
    }

    /**
     *
     * @param url
     * @return Jsoup Document representing the page requested
     * @throws Exception
     */
    protected Document getPage(String url) throws Exception {
        return getConnection(url).get();
    }

    private Connection getConnection(String url) {
        return Jsoup.connect(url).validateTLSCertificates(false).cookies(sessionCookies.get());
    }

    /**
     * Team IDs are not known to end-users and have to be looked up
     */
    public String lookupTeamId() throws Exception {
        return lookupTeamUrl().split("/")[5];
    }

    /**
     * Returns the page that holds team information for the currently logged in user
     */
    public String lookupTeamUrl() throws Exception {
        Document doc = getPage(homeURl);
        return homeURl + doc.select("nav").select("li").get(1).select("a").attr("href");

    }

    /**
     *
     * @return
     * @throws Exception
     */
    public Team getTeam() throws Exception {
        Team team = new Team();

        Document doc = getPage(lookupTeamUrl());
        String teamName = doc.select(".ismSection3").text();
        team.setName(teamName);
        team.setPlayers(parsePlayers(doc.select(".ismPlayerContainer")));

        return team;
    }

    protected ArrayList<Player> parsePlayers(Elements elements) throws Exception {
        ArrayList<Player> players = new ArrayList<Player>();
        for (Element e : elements) {
            players.add(parsePlayer(e));
        }
        return players;

       /* <div class="ismPlayerContainer"> <div class="ismShirtContainer JS_ISM_SHIRT"> <!-- shirt -->  <img src="http://cdn.ismfg.net/static/plfpl/img/shirts/shirt_4_1.png" alt="" width="48" height="63" title="Chelsea" class="ismShirt">  </div> <div class="ismElementControls"> <!-- out --> <span class="ismRemove"></span>  <!-- sub --> <span class="JS_ISM_SUB"> <img src="http://cdn.ismfg.net/static/plfpl/img/icons/sub.png" alt="Substitute player" title="Substitute player" height="16" width="16" class="ismSub ismHide"> <img src="http://cdn.ismfg.net/static/plfpl/img/icons/subx.png" alt="Cancel substitution" title="Cancel substituion" height="16" width="16" class="ismSubx ismHide"> </span> <!-- captain --> <span class="JS_ISM_CAPTAIN"> <img width="16" height="16" alt="Captain" src="http://cdn.ismfg.net/static/plfpl/img/icons/captain.png" title="Captain" class="ismjs-capicon ismCaptain ismCaptainOff"> <img width="16" height="16" alt="Vice-captain" src="http://cdn.ismfg.net/static/plfpl/img/icons/vice_captain.png" title="Vice-captain" class="ismjs-capicon ismViceCaptain ismViceCaptainOff"> </span>  <!-- info -->  <span class="JS_ISM_INFO"> <a href="#77" class="ismInfo ismViewProfile JS_ISM_INFO" title=""><img src="http://cdn.ismfg.net/static/plfpl/img/icons/infowarn.png" alt="Player information " width="16" height="16"></a> </span>  <!-- dream team --> <span class="JS_ISM_DREAMTEAM">  </span> </div> <dl class="ismElementDetail"> <dt>
       <span class="ismElementText ismPitchWebName JS_ISM_NAME"> Courtois </span></dt> <dd> <span class="ismElementText ismPitchStat">

        <a href="#" class="ismTooltip" title="">-2</a>

        </span> </dd> </dl> </div>  */
    }

    protected Player parsePlayer(Element playerNode) {
        Player player = new Player();
        try {
            //player.setClub();
            //player.setShirtUrl();
            player.setLastName(playerNode.select(".JS_ISM_NAME").text());
            String s = playerNode.select(".ismPitchStat").select("a").text();
            if (StringUtil.isNumeric(s)) {
                player.setPoints(Integer.parseInt(s));
            }

        } catch (Exception e) {
            System.out.println("Player is " + player.getLastName());
            e.printStackTrace();
        }

        return player;
    }


}
