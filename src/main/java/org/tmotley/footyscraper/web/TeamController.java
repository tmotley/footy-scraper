package org.tmotley.footyscraper.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.tmotley.footyscraper.auth.FootySiteAuthToken;
import org.tmotley.footyscraper.domain.Team;
import org.tmotley.footyscraper.services.FootyDataService;

import java.security.Principal;

/**
 * Created by Tom on 8/9/15.
 */
@Controller
public class TeamController {
    private FootyDataService dataService;

    @Autowired
    public TeamController(FootyDataService dataService) {
        this.dataService = dataService;
    }

    @RequestMapping(value = "/team", method = RequestMethod.GET)
    public String processTeamRequest(Model model, FootySiteAuthToken principal) throws Exception {
        System.out.println("in team controller!!!!!!");
        System.out.println("Name of user: " + principal.getName());
        Team team = null;
        try {
            dataService.initSession(principal.getAuthCookies());
            team = dataService.getTeam();
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            e.printStackTrace();
            return "error/general";
        } finally {
            dataService.endSession();
        }
        model.addAttribute("team", team);
        return "team";
    }
}
