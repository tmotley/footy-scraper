package org.tmotley.footyscraper.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.tmotley.footyscraper.auth.FootySiteAuthToken;
import org.tmotley.footyscraper.domain.Team;
import org.tmotley.footyscraper.services.FootyDataService;

@Controller
public class HomeController {
	private FootyDataService dataService;

	@Autowired
	public HomeController(FootyDataService dataService) {
		this.dataService = dataService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model, FootySiteAuthToken principal) throws Exception {
		//model.addAttribute(new WebsiteCredentials());
		System.out.println("Name of user: " + principal.getName());
		dataService.initSession(principal.getAuthCookies());
		Team team = dataService.getTeam();
		model.addAttribute(team);
		return "home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getSignInForm() {
		System.out.println("Returning sign-in page...");
		return "login";
	}
}
