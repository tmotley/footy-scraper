package org.tmotley.footyscraper.domain;

/**
 * Created by Tom on 8/8/15.
 */
public class Player {
    private String id;
    private String firstName;
    private String lastName;
    private int matchDayNumber;
    private PlayerPosition position;
    private boolean selected;
    private boolean available;
    private int points;

    public Player() {}

    public Player(String id, String firstName, String lastName, int matchDayNumber, PlayerPosition position, boolean selected, boolean available) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.matchDayNumber = matchDayNumber;
        this.position = position;
        this.selected = selected;
        this.available = available;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getMatchDayNumber() {
        return matchDayNumber;
    }

    public void setMatchDayNumber(int matchDayNumber) {
        this.matchDayNumber = matchDayNumber;
    }

    public PlayerPosition getPosition() {
        return position;
    }

    public void setPosition(PlayerPosition position) {
        this.position = position;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
