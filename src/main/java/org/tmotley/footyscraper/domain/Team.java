package org.tmotley.footyscraper.domain;

import java.util.ArrayList;

/**
 * Created by Tom on 8/8/15.
 */
public class Team {
    String name;
    ArrayList<Player> players = new ArrayList<Player>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addPlayer(Player player) {
        this.players.add(player.getMatchDayNumber(), player);
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }
}
