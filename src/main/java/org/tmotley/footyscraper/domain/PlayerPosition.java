package org.tmotley.footyscraper.domain;

/**
 * Created by Tom on 8/8/15.
 */
public enum PlayerPosition {
    GOALKEEPER, DEFENDER, MIDFIELD, FORWARD
}
